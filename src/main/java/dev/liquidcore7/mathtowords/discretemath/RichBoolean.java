package dev.liquidcore7.mathtowords.discretemath;

public interface RichBoolean {

    /**
     * @see dev.liquidcore7.mathtowords.discretemath.LogicalOperations#eitherOrBoth(boolean, boolean)
     */
    RichBoolean or(RichBoolean that);
    RichBoolean or(boolean that);

    /**
     * @see dev.liquidcore7.mathtowords.discretemath.LogicalOperations#both(boolean, boolean)
     */
    RichBoolean and(RichBoolean that);
    RichBoolean and(boolean that);

    /**
     * @see dev.liquidcore7.mathtowords.discretemath.LogicalOperations#eitherButNotBoth(boolean, boolean)
     */
    RichBoolean notSameAs(RichBoolean that);
    RichBoolean notSameAs(boolean that);

    /**
     * @see dev.liquidcore7.mathtowords.discretemath.LogicalOperations#not(boolean)
     */
    RichBoolean negate();

    /**
     * @see dev.liquidcore7.mathtowords.discretemath.LogicalOperations#none(boolean, boolean)
     */
    RichBoolean nor(RichBoolean that);
    RichBoolean nor(boolean that);

    /**
     * @see dev.liquidcore7.mathtowords.discretemath.LogicalOperations#noneOrBoth(boolean, boolean)
     */
    RichBoolean sameAs(RichBoolean that);
    RichBoolean sameAs(boolean that);

    /**
     * @return a boolean representation of this wrapper
     */
    boolean toBoolean();
}
