package dev.liquidcore7.mathtowords.discretemath;


/**
 * A wrapper for boolean to support meaningful operation names
 */
public class RichBooleans {

    private final static RichBoolean True = new RichBoolean() {
        @Override
        public RichBoolean or(RichBoolean that) {
            return this;
        }

        @Override
        public RichBoolean or(boolean that) {
            return this;
        }

        @Override
        public RichBoolean and(RichBoolean that) {
            return that;
        }

        @Override
        public RichBoolean and(boolean that) {
            return that ? this : False;
        }

        @Override
        public RichBoolean notSameAs(RichBoolean that) {
            return that == False ? this : False;
        }

        @Override
        public RichBoolean notSameAs(boolean that) {
            return that ? this : False;
        }

        @Override
        public RichBoolean negate() {
            return False;
        }

        @Override
        public RichBoolean nor(RichBoolean that) {
            return False;
        }

        @Override
        public RichBoolean nor(boolean that) {
            return False;
        }

        @Override
        public RichBoolean sameAs(RichBoolean that) {
            return that == this ? this : False;
        }

        @Override
        public RichBoolean sameAs(boolean that) {
            return that ? this : False;
        }

        @Override
        public boolean toBoolean() {
            return true;
        }
    };


    private final static RichBoolean False = new RichBoolean() {
        @Override
        public RichBoolean or(RichBoolean that) {
            return that;
        }

        @Override
        public RichBoolean or(boolean that) {
            return that ? True : this;
        }

        @Override
        public RichBoolean and(RichBoolean that) {
            return this;
        }

        @Override
        public RichBoolean and(boolean that) {
            return this;
        }

        @Override
        public RichBoolean notSameAs(RichBoolean that) {
            return that == True ? that : this;
        }

        @Override
        public RichBoolean notSameAs(boolean that) {
            return that ? this : True;
        }

        @Override
        public RichBoolean negate() {
            return True;
        }

        @Override
        public RichBoolean nor(RichBoolean that) {
            return that == this ? True : this;
        }

        @Override
        public RichBoolean nor(boolean that) {
            return that ? this : True;
        }

        @Override
        public RichBoolean sameAs(RichBoolean that) {
            return that == this ? True : this;
        }

        @Override
        public RichBoolean sameAs(boolean that) {
            return that ? this : True;
        }

        public boolean toBoolean() {
            return false;
        }

    };



    public static RichBoolean True() {
        return True;
    }

    public static RichBoolean False() {
        return False;
    }

    public static RichBoolean fromBoolean(boolean value) {
        return value ? True : False;
    }

}
