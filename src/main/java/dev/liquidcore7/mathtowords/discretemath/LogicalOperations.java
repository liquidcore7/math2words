package dev.liquidcore7.mathtowords.discretemath;


/**
 * Represents logical operations using words instead of symbols
 */
public class LogicalOperations {

    /**
     * Performs logical XOR of a and b
     * @return result of a XOR b
     * @see <a href="https://en.wikipedia.org/wiki/Exclusive_or">XOR on Wikipedia</a>
     */
    static boolean eitherButNotBoth(boolean a, boolean b) {
        return a != b;
    }

    /**
     * Performs logical OR of a and b
     * @return result of a OR b
     * @see <a href="https://en.wikipedia.org/wiki/Logical_disjunction">OR on Wikipedia</a>
     */
    static boolean eitherOrBoth(boolean a, boolean b) {
        return a | b;
    }

    /**
     * Performs logical negation of a
     * @return result of NOT b
     * @see <a href="https://en.wikipedia.org/wiki/Negation">NOT on Wikipedia</a>
     */
    static boolean not(boolean a) {
        return !a;
    }

    /**
     * Performs logical AND of a and b
     * @return result of a AND b
     * @see <a href="https://en.wikipedia.org/wiki/Logical_conjunction">AND on Wikipedia</a>
     */
    static boolean both(boolean a, boolean b) {
        return a & b;
    }

    /**
     * Performs logical XNOR of a and b
     * @return result of a XNOR b
     * @see <a href="https://en.wikipedia.org/wiki/XNOR_gate">XNOR on Wikipedia</a>
     */
    static boolean noneOrBoth(boolean a, boolean b) {
        return a == b;
    }

    /**
     * Performs logical NOR of a and b
     * @return result of a NOR b
     * @see <a href="https://en.wikipedia.org/wiki/Inverter_(logic_gate)">NOR on Wikipedia</a>
     */
    static boolean none(boolean a, boolean b) {
        return !a && !b;
    }

}
