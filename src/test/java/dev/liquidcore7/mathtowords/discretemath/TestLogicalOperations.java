package dev.liquidcore7.mathtowords.discretemath;

import dev.liquidcore7.mathtowords.discretemath.helpers.BinaryFunctionDomainTable;
import dev.liquidcore7.mathtowords.discretemath.helpers.DomainTable;
import dev.liquidcore7.mathtowords.discretemath.helpers.UnaryFunctionDomainTable;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TestLogicalOperations {

    @Test
    @DisplayName("a XOR b")
    void textXor() {
        DomainTable domainTable = DomainTable.builder()
                .withArguments(false, false).shouldBe(false)
                .withArguments(true, false).shouldBe(true)
                .withArguments(false, true).shouldBe(true)
                .withArguments(true, true).shouldBe(false)
                .build();
        assertTrue(new BinaryFunctionDomainTable(domainTable)
                .check(LogicalOperations::eitherButNotBoth)
        );
    }

    @Test
    @DisplayName("a OR b")
    void testOr() {
        DomainTable domainTable = DomainTable.builder()
                .withArguments(false, false).shouldBe(false)
                .withArguments(true, false).shouldBe(true)
                .withArguments(false, true).shouldBe(true)
                .withArguments(true, true).shouldBe(true)
                .build();
        assertTrue(
                new BinaryFunctionDomainTable(domainTable)
                .check(LogicalOperations::eitherOrBoth)
        );
    }

    @Test
    @DisplayName("a AND b")
    void testAnd() {
        DomainTable domainTable = DomainTable.builder()
                .withArguments(false, false).shouldBe(false)
                .withArguments(true, false).shouldBe(false)
                .withArguments(false, true).shouldBe(false)
                .withArguments(true, true).shouldBe(true)
                .build();
        assertTrue(
                new BinaryFunctionDomainTable(domainTable)
                        .check(LogicalOperations::both)
        );
    }

    @Test
    @DisplayName("a XNOR b")
    void testXnor() {
        DomainTable domainTable = DomainTable.builder()
                .withArguments(false, false).shouldBe(true)
                .withArguments(true, false).shouldBe(false)
                .withArguments(false, true).shouldBe(false)
                .withArguments(true, true).shouldBe(true)
                .build();
        assertTrue(
                new BinaryFunctionDomainTable(domainTable)
                        .check(LogicalOperations::noneOrBoth)
        );
    }

    @Test
    @DisplayName("a NOR b")
    void testNor() {
        DomainTable domainTable = DomainTable.builder()
                .withArguments(false, false).shouldBe(true)
                .withArguments(true, false).shouldBe(false)
                .withArguments(false, true).shouldBe(false)
                .withArguments(true, true).shouldBe(false)
                .build();
        assertTrue(
                new BinaryFunctionDomainTable(domainTable)
                        .check(LogicalOperations::none)
        );
    }

    @Test
    @DisplayName("NOT a")
    void testNot() {
        DomainTable domainTable = DomainTable.builder()
                .withArguments(true).shouldBe(false)
                .withArguments(false).shouldBe(true)
                .build();
        assertTrue(
                new UnaryFunctionDomainTable(domainTable)
                .check(LogicalOperations::not)
        );
    }


}
