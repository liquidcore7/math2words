package dev.liquidcore7.mathtowords.discretemath;

import dev.liquidcore7.mathtowords.discretemath.helpers.BinaryFunctionDomainTable;
import dev.liquidcore7.mathtowords.discretemath.helpers.DomainTable;
import dev.liquidcore7.mathtowords.discretemath.helpers.UnaryFunctionDomainTable;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Supplier;

import static dev.liquidcore7.mathtowords.discretemath.RichBooleans.False;
import static dev.liquidcore7.mathtowords.discretemath.RichBooleans.True;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TestRichBoolean {

    private BiFunction<Boolean, Boolean, Boolean> lift(
            Function<RichBoolean, RichBoolean> truthFunctor,
            Function<RichBoolean, RichBoolean> lieFunctor) {
        return (x, y) -> {
            Function<RichBoolean, RichBoolean> appropriateFunctor = x ? truthFunctor : lieFunctor;
            return appropriateFunctor
                    .compose(RichBooleans::fromBoolean)
                    .andThen(RichBoolean::toBoolean)
                    .apply(y);
        };
    }

    private Function<Boolean, Boolean> lift(
            Supplier<RichBoolean> truthFunctor,
            Supplier<RichBoolean> lieFunctor) {
        return x -> {
            Supplier<RichBoolean> appropriateFunctor = x ? truthFunctor : lieFunctor;
            return appropriateFunctor.get().toBoolean();
        };
    }

    @Test
    @DisplayName("a XOR b")
    void textXor() {
        DomainTable domainTable = DomainTable.builder()
                .withArguments(false, false).shouldBe(false)
                .withArguments(true, false).shouldBe(true)
                .withArguments(false, true).shouldBe(true)
                .withArguments(true, true).shouldBe(false)
                .build();
        assertTrue(new BinaryFunctionDomainTable(domainTable)
                .check(lift(
                        True()::notSameAs,
                        False()::notSameAs
                ))
        );
    }

    @Test
    @DisplayName("a OR b")
    void testOr() {
        DomainTable domainTable = DomainTable.builder()
                .withArguments(false, false).shouldBe(false)
                .withArguments(true, false).shouldBe(true)
                .withArguments(false, true).shouldBe(true)
                .withArguments(true, true).shouldBe(true)
                .build();
        assertTrue(
                new BinaryFunctionDomainTable(domainTable)
                        .check(lift(
                                True()::or,
                                False()::or
                        ))
        );
    }

    @Test
    @DisplayName("a AND b")
    void testAnd() {
        DomainTable domainTable = DomainTable.builder()
                .withArguments(false, false).shouldBe(false)
                .withArguments(true, false).shouldBe(false)
                .withArguments(false, true).shouldBe(false)
                .withArguments(true, true).shouldBe(true)
                .build();
        assertTrue(
                new BinaryFunctionDomainTable(domainTable)
                        .check(lift(
                                True()::and,
                                False()::and
                        ))
        );
    }

    @Test
    @DisplayName("a XNOR b")
    void testXnor() {
        DomainTable domainTable = DomainTable.builder()
                .withArguments(false, false).shouldBe(true)
                .withArguments(true, false).shouldBe(false)
                .withArguments(false, true).shouldBe(false)
                .withArguments(true, true).shouldBe(true)
                .build();
        assertTrue(
                new BinaryFunctionDomainTable(domainTable)
                        .check(lift(
                                True()::sameAs,
                                False()::sameAs
                        ))
        );
    }

    @Test
    @DisplayName("a NOR b")
    void testNor() {
        DomainTable domainTable = DomainTable.builder()
                .withArguments(false, false).shouldBe(true)
                .withArguments(true, false).shouldBe(false)
                .withArguments(false, true).shouldBe(false)
                .withArguments(true, true).shouldBe(false)
                .build();
        assertTrue(
                new BinaryFunctionDomainTable(domainTable)
                        .check(lift(
                                True()::nor,
                                False()::nor
                        ))
        );
    }

    @Test
    @DisplayName("NOT a")
    void testNot() {
        DomainTable domainTable = DomainTable.builder()
                .withArguments(true).shouldBe(false)
                .withArguments(false).shouldBe(true)
                .build();
        assertTrue(
                new UnaryFunctionDomainTable(domainTable)
                        .check(lift(
                                True()::negate,
                                False()::negate
                        ))
        );
    }
}
