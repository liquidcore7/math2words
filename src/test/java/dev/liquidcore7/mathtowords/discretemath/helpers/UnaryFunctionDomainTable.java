package dev.liquidcore7.mathtowords.discretemath.helpers;

import java.util.Collection;
import java.util.function.Function;

public class UnaryFunctionDomainTable
        extends FunctionDomainTable<Function<Boolean, Boolean>> {

    public UnaryFunctionDomainTable(DomainTable domainTable) {
        super(domainTable);
    }

    @Override
    protected int arity() {
        return 1;
    }

    @Override
    protected Boolean extractArgumentsAndApply(Function<Boolean, Boolean> function, Collection<Boolean> arguments) {
        return function.apply( arguments.iterator().next() );
    }
}
