package dev.liquidcore7.mathtowords.discretemath.helpers;

import java.util.Collection;
import java.util.Iterator;
import java.util.function.BiFunction;

public class BinaryFunctionDomainTable
        extends FunctionDomainTable<BiFunction<Boolean, Boolean, Boolean>> {

    public BinaryFunctionDomainTable(DomainTable domainTable) {
        super(domainTable);
    }

    @Override
    protected int arity() {
        return 2;
    }

    @Override
    protected Boolean extractArgumentsAndApply(BiFunction<Boolean, Boolean, Boolean> function, Collection<Boolean> arguments) {
        Iterator<Boolean> argIterator = arguments.iterator();
        Boolean x = argIterator.next();
        Boolean y = argIterator.next();
        return function.apply(x, y);
    }
}
