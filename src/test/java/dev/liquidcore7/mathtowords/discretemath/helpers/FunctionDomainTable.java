package dev.liquidcore7.mathtowords.discretemath.helpers;

import java.util.Collection;

public abstract class FunctionDomainTable<FuncT> {
    private DomainTable domainTable;

    public FunctionDomainTable(DomainTable domainTable) {
        this.domainTable = domainTable;
    }

    protected abstract int arity();
    protected abstract Boolean extractArgumentsAndApply(FuncT function, Collection<Boolean> arguments);

    private Boolean apply(FuncT function, Collection<Boolean> arguments) throws IllegalArgumentException {
        if (arguments.size() != arity()) {
            throw new IllegalArgumentException("Function and argument arities do not match!");
        }
        return extractArgumentsAndApply(function, arguments);
    }

    public boolean check(FuncT function) {
        return domainTable.getDomain()
                .stream()
                .allMatch(argumentsResultPair ->
                        apply(function, argumentsResultPair.getKey()) == argumentsResultPair.getValue()
                        );
    }
}
