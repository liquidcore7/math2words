package dev.liquidcore7.mathtowords.discretemath.helpers;

import java.util.*;
import java.util.stream.Collectors;

public class DomainTable {
    private Map<Collection<Boolean>, Boolean> fnDomain;

    private DomainTable(Map<Collection<Boolean>, Boolean> domain) {
        this.fnDomain = domain;
    }

    public interface DomainTableArguments {
        DomainTableBuilder shouldBe(Boolean result);
    }

    public interface DomainTableBuilder {
        DomainTableArguments withArguments(Boolean head, Boolean... tail);
        DomainTable build();
    }

    public static DomainTableBuilder builder() {
        return new DomainTableBuilder() {

            private Map<Collection<Boolean>, Boolean> cachedMap = new HashMap<>();

            @Override
            public DomainTableArguments withArguments(Boolean head, Boolean... tail) {
                List<Boolean> arguments = Arrays.stream(tail).collect(Collectors.toList());
                arguments.add(0, head);

                DomainTableBuilder outerRef = this;

                return new DomainTableArguments() {
                    @Override
                    public DomainTableBuilder shouldBe(Boolean result) {
                        cachedMap.put(arguments, result);
                        return outerRef;
                    }
                };
            }

            @Override
            public DomainTable build() {
                return new DomainTable(cachedMap);
            }
        };
    }

    public Set<Map.Entry<Collection<Boolean>, Boolean>> getDomain() {
        return fnDomain.entrySet();
    }

}
